#ifndef __IFMO_DISTRIBUTED_CLASS_LAMPORT__H
#define __IFMO_DISTRIBUTED_CLASS_LAMPORT__H

#include "ipc.h"

timestamp_t get_lamport_time();

void set_lamport_time(timestamp_t new_time);

void increment_lamport_time();

#endif //__IFMO_DISTRIBUTED_CLASS_LAMPORT__H
