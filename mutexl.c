#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include "pa4.h"
#include "worker.h"
#include "lamport.h"
#include "ipc.h"
#include "mutexl.h"

typedef struct {
    local_id worker_id;
    timestamp_t timestamp;
} Request;

typedef struct RequestNode {
    Request request;
    struct RequestNode* next;
} RequestNode;

static RequestNode* request_queue_root = NULL;

static void add_request(const local_id worker_id, const timestamp_t timestamp) {
    RequestNode* request_node = malloc(sizeof(RequestNode));

    if (
            request_queue_root == NULL ||
            request_queue_root->request.timestamp > timestamp ||
            (request_queue_root->request.timestamp == timestamp && request_queue_root->request.worker_id > worker_id)
    ) {
        *request_node = (RequestNode) {
                .request = {
                        .worker_id = worker_id,
                        .timestamp = timestamp
                },
                .next = request_queue_root
        };
        request_queue_root = request_node;
        return;
    }

    RequestNode* node = request_queue_root;
    while (
            node->next != NULL && (
                    node->next->request.timestamp < timestamp ||
                    (node->next->request.timestamp == timestamp && node->next->request.worker_id < worker_id)
            )
    ) {
        node = node->next;
    }
    *request_node = (RequestNode) {
            .request = {
                    .worker_id = worker_id,
                    .timestamp = timestamp
            },
            .next = node->next
    };
    node->next = request_node;
}

static void remove_request(const local_id worker_id) {
    if (request_queue_root == NULL) {
        return;
    } else if (request_queue_root->request.worker_id == worker_id) {
        RequestNode* removed_node = request_queue_root;
        request_queue_root = removed_node->next;
        free(removed_node);
        return;
    }
    RequestNode* node = request_queue_root;
    while (node->next != NULL) {
        if (node->next->request.worker_id == worker_id) {
            RequestNode* removed_node = node->next;
            node->next = removed_node->next;
            free(removed_node);
            return;
        }
    }
}

int handle_cs_message(const Worker* worker, local_id from, const Message* message) {
    switch (message->s_header.s_type) {
        case CS_REQUEST:
            add_request(from, message->s_header.s_local_time);
            increment_lamport_time();
            Message reply_message = {
                    .s_header = {
                            .s_magic = MESSAGE_MAGIC,
                            .s_payload_len = 0,
                            .s_type = CS_REPLY,
                            .s_local_time = get_lamport_time()
                    }
            };
            send((void*) worker, from, &reply_message);
            break;

        case CS_RELEASE:
            remove_request(from);
            break;

        default:
            return -1;
    }

    return 0;
}

int request_cs(const void * self) {
    Worker* worker = (Worker*) self;

    increment_lamport_time();
    timestamp_t request_timestamp = get_lamport_time();
    add_request(worker->id, request_timestamp);

    Message request_message = {
            .s_header = {
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = 0,
                    .s_type = CS_REQUEST,
                    .s_local_time = request_timestamp
            }
    };
    send_multicast(worker, &request_message);

    int received_workers_count = 0;
    int* received_workers = calloc((size_t) worker->worker_pipes_count, sizeof(int));
    Message* received_message = malloc(sizeof(Message));

    for (int i = 0; i < worker->worker_pipes_count; i++) {
        if (
                get_skipped_message(worker->worker_pipes[i].id, CS_REQUEST, received_message) == 0 ||
                get_skipped_message(worker->worker_pipes[i].id, CS_RELEASE, received_message) == 0
        ) {
            handle_cs_message(worker, worker->worker_pipes[i].id, received_message);
        }
    }

    while (request_queue_root->request.worker_id != worker->id || received_workers_count < worker->worker_pipes_count) {
        for (local_id i = 0; i < worker->worker_pipes_count; i++) {
            if (receive((void *) worker, worker->worker_pipes[i].id, received_message) == 0) {
                timestamp_t received_time = received_message->s_header.s_local_time;
                if (get_lamport_time() < received_time) {
                    set_lamport_time(received_time);
                }
                increment_lamport_time();

                if (received_message->s_header.s_type == CS_REPLY) {
                    if (!received_workers[i] && received_time > request_timestamp) {
                        received_workers[i] = 1;
                        received_workers_count++;
                    }
                } else if (handle_cs_message(worker, worker->worker_pipes[i].id, received_message) != 0) {
                    skip_message(worker->worker_pipes[i].id, received_message);
                }
            }
        }
    }

    free(received_workers);
    free(received_message);

    return 0;
}

int release_cs(const void * self) {
    Worker* worker = (Worker*) self;

    increment_lamport_time();
    remove_request(worker->id);
    Message release_message = {
            .s_header = {
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = 0,
                    .s_type = CS_RELEASE,
                    .s_local_time = get_lamport_time()
            }
    };
    send_multicast(worker, &release_message);

    return 0;
}
