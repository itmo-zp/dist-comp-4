#include <stdio.h>
#include <unistd.h>
#include "log.h"
#include "pa1.h"
#include "worker.h"

void log_pipe(const ProcessPipe* pipe_from, const ProcessPipe* pipe_to) {
    printf(
            log_pipe_fmt,
            pipe_to->id, pipe_from->id,
            pipe_from->in, pipe_to->in,
            pipe_from->out, pipe_to->out
    );
    fprintf(
            pipe_log_file,
            log_pipe_fmt,
            pipe_to->id, pipe_from->id,
            pipe_from->in, pipe_to->in,
            pipe_from->out, pipe_to->out
    );
    fflush(pipe_log_file);
}

void log_started(const Worker* worker) {
    printf(log_started_fmt, worker->id, getpid(), getppid());
    fprintf(event_log_file, log_started_fmt, worker->id, getpid(), getppid());
    fflush(event_log_file);
}

void log_received_all_started(const Worker* worker) {
    printf(log_received_all_started_fmt, worker->id);
    fprintf(event_log_file, log_received_all_started_fmt, worker->id);
    fflush(event_log_file);
}

void log_done(const Worker* worker) {
    printf(log_done_fmt, worker->id);
    fprintf(event_log_file, log_done_fmt, worker->id);
    fflush(event_log_file);
}

void log_received_all_done(const Worker* worker) {
    printf(log_received_all_done_fmt, worker->id);
    fprintf(event_log_file, log_received_all_done_fmt, worker->id);
    fflush(event_log_file);
}
