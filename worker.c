#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include "worker.h"
#include "ipc.h"
#include "log.h"
#include "lamport.h"
#include "pa2345.h"
#include "pa4.h"
#include "mutexl.h"

typedef struct SkippedMessageNode {
    Message message;
    local_id from;
    struct SkippedMessageNode* next;
} SkippedMessageNode;

static SkippedMessageNode* skipped_message_queue_root = NULL;

static void wait_workers(const Worker *worker, const MessageType type) {
    int received_workers_count = 0;
    int* received_workers = calloc((size_t) worker->worker_pipes_count, sizeof(int));
    Message* received_message = malloc(sizeof(Message));

    for (int i = 0; i < worker->worker_pipes_count; i++) {
        if (get_skipped_message(worker->worker_pipes[i].id, type, received_message) == 0) {
            received_workers[i] = 1;
            received_workers_count++;
        }
    }
    while (received_workers_count < worker->worker_pipes_count) {
        for (int i = 0; i < worker->worker_pipes_count; i++) {
            if (receive((void *) worker, worker->worker_pipes[i].id, received_message) == 0) {
                timestamp_t received_time = received_message->s_header.s_local_time;
                if (get_lamport_time() < received_time) {
                    set_lamport_time(received_time);
                }
                increment_lamport_time();
                if (!received_workers[i] && received_message->s_header.s_type == type) {
                    received_workers[i] = 1;
                    received_workers_count++;
                } else if (handle_cs_message(worker, worker->worker_pipes[i].id, received_message) != 0) {
                    skip_message(worker->worker_pipes[i].id, received_message);
                }
            }
        }
    }

    free(received_workers);
    free(received_message);
}

static void prepare(const Worker* worker) {
    increment_lamport_time();
    Message message = {
            .s_header = {
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = 48,
                    .s_type = STARTED,
                    .s_local_time = get_lamport_time()
            }
    };
    send_multicast((void *) worker, &message);

    wait_workers(worker, STARTED);
}

static void do_work(const Worker *worker) {
    int operations_count = worker->id * 5;
    int mut_exl = get_mut_exl();
    for (int i = 1; i <= operations_count; i++) {
        if (mut_exl) {
            request_cs(worker);
        }
        char str[50];
        strcpy(str, log_loop_operation_fmt);
        sprintf(str, log_loop_operation_fmt, worker->id, i, operations_count);
        print(str);
        if (mut_exl) {
            release_cs(worker);
        }
    }
}

static void finish(const Worker* worker) {
    increment_lamport_time();
    Message message = {
            .s_header = {
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = 28,
                    .s_type = DONE,
                    .s_local_time = get_lamport_time()
            }
    };
    send_multicast((void *) worker, &message);

    wait_workers(worker, DONE);
}

int start(const Worker* worker) {
    log_started(worker);
    prepare(worker);
    log_received_all_started(worker);
    do_work(worker);
    log_done(worker);
    finish(worker);
    log_received_all_done(worker);

    return 0;
}

void skip_message(const local_id from, const Message* message) {
    SkippedMessageNode* skipped_message_node = malloc(sizeof(SkippedMessageNode));
    *skipped_message_node = (SkippedMessageNode) {
            .message = *message,
            .from = from,
            .next = NULL
    };

    if (skipped_message_queue_root == NULL) {
        skipped_message_queue_root = skipped_message_node;
        return;
    }
    SkippedMessageNode* node = skipped_message_queue_root;
    while (node->next != NULL) {
        node = node->next;
    }
    node->next = skipped_message_node;
}

int get_skipped_message(const local_id from, const int16_t type, Message* msg) {
    if (skipped_message_queue_root == NULL) {
        return -1;
    } else if (skipped_message_queue_root->from == from && skipped_message_queue_root->message.s_header.s_type == type) {
        SkippedMessageNode* removed_node = skipped_message_queue_root;
        *msg = removed_node->message;
        skipped_message_queue_root = removed_node->next;
        free(removed_node);
        return 0;
    }
    SkippedMessageNode* node = skipped_message_queue_root;
    while (node->next != NULL) {
        if (node->next->from == from && node->next->message.s_header.s_type == type) {
            SkippedMessageNode* removed_node = node->next;
            *msg = removed_node->message;
            node->next = removed_node->next;
            free(removed_node);
            return 0;
        }
        node = node ->next;
    }

    return -1;
}
