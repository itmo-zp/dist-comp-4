#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "worker.h"
#include "log.h"
#include "common.h"
#include "pa4.h"

FILE* event_log_file;
FILE* pipe_log_file;

static int initWorkers(local_id proc_count);

static int mut_exl = 0;

int get_mut_exl() {
    return mut_exl;
}

int main(int argc, char** argv) {
	local_id proc_count = 0;

	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-p") == 0) {
            if (++i >= argc) {
                break;
            }
			proc_count = (local_id) strtol(argv[i], NULL, 10);
			if (errno != 0 || proc_count < 1 || proc_count > MAX_WORKERS_COUNT) {
				printf("Illegal process count: should be [1; 10]\n");
				return 1;
			}
		} else if (strcmp(argv[i], "--mutexl") == 0) {
		    mut_exl = 1;
		} else {
			printf("Illegal option %s\n", argv[i]);
			return 1;
		}
	}

	if (proc_count == 0) {
		printf("Usage: %s -p X [--mutexl]\n", argv[0]);
		return 1;
	}

    event_log_file = fopen(events_log, "w");
    if (event_log_file == NULL) {
        perror("fopen");
        return 1;
    }
    pipe_log_file = fopen(pipes_log, "w");
    if (pipe_log_file == NULL) {
        perror("fopen");
        return 1;
    }

	return initWorkers(proc_count);
}

int initWorkers(const local_id proc_count) {
    Worker* workers = malloc(proc_count * sizeof(Worker));
    ProcessPipe* master_pipes = malloc(proc_count * sizeof(ProcessPipe));
    int pipe_to[2], pipe_from[2];

    for (local_id worker_index = 0; worker_index < proc_count; worker_index++) {
        if(pipe(pipe_to) == -1 || pipe(pipe_from) == -1) {
            perror("pipe");
            return 1;
        }
        if (
                fcntl(pipe_to[0], F_SETFL, fcntl(pipe_to[0], F_GETFL) | O_NONBLOCK)
                || fcntl(pipe_from[0], F_SETFL, fcntl(pipe_from[0], F_GETFL) | O_NONBLOCK)
                || fcntl(pipe_to[1], F_SETFL, fcntl(pipe_to[1], F_GETFL) | O_NONBLOCK)
                || fcntl(pipe_from[1], F_SETFL, fcntl(pipe_from[1], F_GETFL) | O_NONBLOCK)
        ) {
            perror("fcntl");
            return 1;
        }
        ProcessPipe worker_pipe = {
                .id = worker_index + (local_id)1,
                .in = pipe_to[0],
                .out = pipe_from[1]
        };
        ProcessPipe master_pipe = {
                .id = PARENT_ID,
                .in = pipe_from[0],
                .out = pipe_to[1]
        };
        master_pipes[worker_index] = worker_pipe;
        workers[worker_index] = (Worker) {
                .id = worker_index + (local_id)1,
                .master_pipe = master_pipe,
                .worker_pipes_count = proc_count - (local_id)1,
                .worker_pipes = malloc((proc_count - 1) * sizeof(ProcessPipe))
        };
        log_pipe(&worker_pipe, &master_pipe);
    }
    for (local_id root_worker_index = 0; root_worker_index < proc_count; root_worker_index++) {
        Worker root_worker = workers[root_worker_index];
        for (local_id worker_index = root_worker_index + (local_id)1; worker_index < proc_count; worker_index++) {
            if(pipe(pipe_to) == -1 || pipe(pipe_from) == -1) {
                perror("pipe");
                return 1;
            }
            if (
                    fcntl(pipe_to[0], F_SETFL, fcntl(pipe_to[0], F_GETFL) | O_NONBLOCK)
                    || fcntl(pipe_from[0], F_SETFL, fcntl(pipe_from[0], F_GETFL) | O_NONBLOCK)
                    || fcntl(pipe_to[1], F_SETFL, fcntl(pipe_to[1], F_GETFL) | O_NONBLOCK)
                    || fcntl(pipe_from[1], F_SETFL, fcntl(pipe_from[1], F_GETFL) | O_NONBLOCK)
                    ) {
                perror("fcntl");
                return 1;
            }
            ProcessPipe proc_pipe_to = {
                    .id = worker_index + (local_id)1,
                    .in = pipe_from[0],
                    .out = pipe_to[1]
            };
            ProcessPipe proc_pipe_from = {
                    .id = root_worker_index + (local_id)1,
                    .in = pipe_to[0],
                    .out = pipe_from[1]
            };
            root_worker.worker_pipes[worker_index - (local_id)1] = proc_pipe_to;
            workers[worker_index].worker_pipes[root_worker_index] = proc_pipe_from;
            log_pipe(&proc_pipe_to, &proc_pipe_from);
        }
    }

    pid_t* worker_pids = malloc(proc_count * sizeof(pid_t));
    for (int i = 0; i < proc_count; i++) {
        pid_t worker_pid;
        if((worker_pid = fork()) == -1) {
            perror("fork");
            return 1;
        }

        if (worker_pid == 0) {
            for (local_id worker_index = 0; worker_index < proc_count; worker_index++) {
                close(master_pipes[worker_index].in);
                close(master_pipes[worker_index].out);
                if (i == worker_index) {
                    continue;
                }
                Worker worker = workers[worker_index];
                for (local_id pipe_index = 0; pipe_index < worker.worker_pipes_count; pipe_index++) {
                    close(worker.worker_pipes[pipe_index].in);
                    close(worker.worker_pipes[pipe_index].out);
                    close(worker.master_pipe.out);
                    close(worker.master_pipe.in);
                }
            }

            return start(&workers[i]);
        } else {
            worker_pids[i] = worker_pid;
            Worker worker = workers[i];
            for (local_id pipe_index = 0; pipe_index < worker.worker_pipes_count; pipe_index++) {
                close(worker.worker_pipes[pipe_index].in);
                close(worker.worker_pipes[pipe_index].out);
                close(worker.master_pipe.out);
                close(worker.master_pipe.in);
            }
        }
    }

    for (int i = 0; i < proc_count; i++) {
        waitpid(worker_pids[i], NULL, 0);
    }

    return 0;
}
